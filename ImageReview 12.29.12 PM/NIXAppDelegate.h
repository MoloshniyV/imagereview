//
//  NIXAppDelegate.h
//  ImageReview
//
//  Created by Admin on 11/1/13.
//  Copyright (c) 2013 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NIXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
