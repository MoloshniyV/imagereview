//
//  main.m
//  ImageReview
//
//  Created by Admin on 11/1/13.
//  Copyright (c) 2013 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NIXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NIXAppDelegate class]));
    }
}
